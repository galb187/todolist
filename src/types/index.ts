export interface ITodo {
    id: string;
    todo: string;
    active: boolean;
    time_added: string;
    due_time: string;
}

export enum ListOption {
    a = "a",
    all = "all",
    o = "o",
    open = "open",
    c = "c",
    completed = "completed",
}

export enum CommandOption {
    add = "add",
    ls = "ls",
    del = "del",
    activate = "activate",
    finish = "finish",
    help = "help",
    deadline = "deadline",
}

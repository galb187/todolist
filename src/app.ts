import { parseArgs } from "./utils/ArgsParser.js";
import { validateArgs } from "./utils/ArgsValidator.js";
import { createDB, stringifyDataAndWriteToFile } from "./db/DB.js";
import { CommandOption, ITodo } from "./types/index.js";
import { readFileReturnDataAsArrayObjects } from "./db/DB.js";
import {addFunction, listFunction, deleteFunction, doneFunction, openFunction, addDeadline} from "./BL/TodoListBL.js";
import {printMenu} from "./view/UI.js";

(async function init() {
  const args: string[] = process.argv;
  
  //parsing arguments
  const selectedOptionsByUser = parseArgs(args);
  // console.log("selectedOptionsByUser after parse: " , selectedOptionsByUser);
  
  //validating arguments
  const isValid = validateArgs(selectedOptionsByUser);
  
  if (isValid) {
    //check if file exists/create it in first time user runs npm run start
    const cwd: string = args[1].slice(0, -11); //slice src/index.js which is 13 chars
    const jsonPathDB = `${cwd}data/todos-data.json`;
    console.log(jsonPathDB);
    
    await createDB(jsonPathDB);
    let currentTodos : ITodo[] = await readFileReturnDataAsArrayObjects(jsonPathDB);        
    switch (selectedOptionsByUser.command) {
      
      
      case CommandOption.add: {
        currentTodos = (await addFunction(selectedOptionsByUser.param, currentTodos)).currentTodos;
        await stringifyDataAndWriteToFile(jsonPathDB, currentTodos);
        break;
      }

      case CommandOption.deadline :{
        const args = selectedOptionsByUser.param.split(" ");
        currentTodos = await addDeadline(args[0],args[1],currentTodos);
        await stringifyDataAndWriteToFile(jsonPathDB, currentTodos);
        break;
      }

            case CommandOption.ls: {
                // console.log("ls");
                currentTodos = await listFunction(
                    selectedOptionsByUser.param,
                    currentTodos
                );
                break;
            }

            case CommandOption.del: {
                // console.log("in delete switch case");
                currentTodos = await deleteFunction(
                    selectedOptionsByUser.param,
                    currentTodos
                );
                await stringifyDataAndWriteToFile(jsonPathDB, currentTodos);
                break;
            }

            case CommandOption.finish: {
                currentTodos = await doneFunction(
                    selectedOptionsByUser.param,
                    currentTodos
                );
                await stringifyDataAndWriteToFile(jsonPathDB, currentTodos);
                break;
            }

            case CommandOption.activate: {
                currentTodos = await openFunction(
                    selectedOptionsByUser.param,
                    currentTodos
                );
                await stringifyDataAndWriteToFile(jsonPathDB, currentTodos);
                break;
            }

            case CommandOption.help: {
                await printMenu();
                break;
            }

            default: {
                // await printMenu();
                // We will display help when no argument is passed or invalid
            }
        }
    }
})();

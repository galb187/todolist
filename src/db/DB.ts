import fs from "fs/promises";
import { constants } from "fs";
import { ITodo } from "../types/index.js";

export async function createDB(path: string) {
    // console.log('db path ',path);

    try {
        await fs.access(path, constants.R_OK && constants.W_OK);
        //no need to create
    } catch {
        //create file
        await fs.writeFile(path, JSON.stringify([]), "utf-8");
    }
}

//read file
export async function readFileReturnDataAsArrayObjects(jsonPathFile: string) {
    const fileInput: string = await fs.readFile(jsonPathFile, "utf-8");
    const data: ITodo[] = JSON.parse(fileInput);
    return data;
}

//write file
export async function stringifyDataAndWriteToFile(
    jsonPathFile: string,
    data: ITodo[]
) {
    const updatedData: string = JSON.stringify(data);
    await fs.writeFile(jsonPathFile, updatedData, "utf-8");
}

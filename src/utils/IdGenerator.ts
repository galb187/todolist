export function generateID() {
    return (
        Date.now().toString(36) + Math.random().toString(36).substr(2)
    ).substr(0, 10);
}

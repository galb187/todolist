export interface IOptions {
    command: string;
    param: string;
}

export function parseArgs(args: string[]): IOptions {
    const slicedArgs = args.slice(2); //remove node and path
    // console.log("slicedArgs: ", slicedArgs);
    const optionsSelectedByUser: IOptions = {
        command: slicedArgs[0],
        param: slicedArgs.slice(1).join(" "),
    };

    return optionsSelectedByUser;
}

import { expect } from "chai";
import {addFunction} from "../src/BL/TodoListBL";
import {readFileReturnDataAsArrayObjects} from "../src/db/DB";
import { ITodo } from "../src/types/index";
const jsonPathDB = "/Users/elirandarshan/Desktop/ToDo-gal/data/todos-data.json";
describe("Testing the add functionality",()=>{  
    context("Adding a new task",()=>{
        let todos : ITodo[]
        let size : number;
        beforeEach(async()=>{
            todos = await readFileReturnDataAsArrayObjects(jsonPathDB);
            size = Object.keys(todos).length;
        })
        it ("Should check that addFunction is a function",()=>{
            expect(addFunction).to.be.instanceOf(Function);
        })
        it("Should check that a new item has been added",async ()=>{
            await addFunction("testing",todos).then((result)=>{
                const new_task = todos.find((task)=> task.id === result.task_id);
                const new_size = Object.keys(todos).length;
                if (new_task){
                    expect(new_task.todo).to.be.equal("testing")
                    expect(new_size).to.be.equal(size + 1);
                } else {
                    throw new Error("Adding has failed")
                }
            })

        })
    })

})
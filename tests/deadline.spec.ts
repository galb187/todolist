import { expect } from "chai";
import {addDeadline} from "../src/BL/TodoListBL";
import {readFileReturnDataAsArrayObjects} from "../src/db/DB";
import { ITodo } from "../src/types/index";
const jsonPathDB = "/Users/elirandarshan/Desktop/ToDo-gal/data/todos-data.json";

describe("Testing the addDeadline functionality",()=>{
    context("adding a new deadline to a given task",()=>{
        let todos : ITodo[]
        beforeEach(async()=>{
            todos = await readFileReturnDataAsArrayObjects(jsonPathDB);
        })
        it("Should add a deadline to task : kxopckj468",()=>{
            const wanted_task = todos.find((task)=>task.id === "kxopckj468");
            if (wanted_task){
                addDeadline("kxopckj468","01/01/2042",todos);
                expect(wanted_task).to.haveOwnProperty("due_time").equal("01/01/2042");
            }
        })
    })
})
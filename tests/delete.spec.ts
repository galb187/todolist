import {readFileReturnDataAsArrayObjects} from "../src/db/DB";
import {deleteFunction} from "../src/BL/TodoListBL";
import { expect } from "chai";
import { ITodo } from "../src/types/index";

describe("Testing Delete functionality", ()=>{
    context("#delete",()=>{
        let todos : ITodo[];
        before("init todos",async ()=> {
            todos = await readFileReturnDataAsArrayObjects(
                "/Users/galbarkan/Desktop/Backend_course_exercises/Typescript/ToDoList/data/todos-data.json");
        });

        it("should exist", ()=>{
            expect(deleteFunction).to.be.instanceOf(Function);
        });

        it("should delete an item by task id ",async ()=>{
            const toDelete = Math.floor(Math.random()*todos.length);
            const idToDelete = todos[toDelete].id;
            const afterDeletion = await deleteFunction(idToDelete,todos);
            const validTaskWasDeleted = afterDeletion.find(x => x.id === idToDelete);
            expect(validTaskWasDeleted).to.be.equal(undefined);
            expect(afterDeletion.length).to.be.equal(todos.length-1);
        });
    });
});
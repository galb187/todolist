[![ts](https://badgen.net/badge/-/node?icon=typescript&label&labelColor=blue&color=555555)](https://github.com/TypeStrong/ts-node)
[<img src="https://img.shields.io/badge/-ESLint-4B32C3.svg?logo=ESlint">](https://eslint.org/)
[![Husky](https://img.shields.io/static/v1?label=Formated+by&message=Prettier&color=ff69b4)](https://github.com/prettier/prettier)
[![Husky](https://img.shields.io/static/v1?label=Hooked+by&message=Husky&color=success)](https://typicode.github.io/husky)
# Node.js TS 
## ESM starter kit

This app will let you the manage your daily tasks by adding, deleting, showing relevant tasks etc.

It is indeed experimental and can change at any time.    
Be sure to use **`.js`** extension name at the end of your import statements of your own source files.

You can tell VSCode to add **.js** extentions for you on auto-imports...    
Simply open the command pallete **`Cmd + Shift + P`**, then type   **`>open settings json`**    
and add these two lines at the root level of the configuration object:

Add Task:
    add "todo Item"     #add new task to list, will be undone by default, insert todo with apostrophes

An easier alternative you may consider is to simply dompile everything to CommonJS.
  
This setup is only relevant if it is important for you to be standard complient with ESNext and specifically with EcmaScript Modules (ESM) running natively in the Node.js runtime. 

Delete Task:
    del TASK_ID            # deleting task by ID (ID can be found by show tasks command)
    del -completed         # deleting all completed tasks from list

### linting
**`eslint`** **`prettier`** and **`husky`** are included


Finish Task:
    finish TASK_ID         #mark task with ID to be completed

Help:
    help             #show help menu to see how to use the app